module Main where

import Lib
import Data.Foldable (find)
import Data.Maybe (isJust)
import Control.Applicative

head' :: [a] -> Maybe a
head' []     = Nothing
head' (x:xs) = Just x

data Move = Rock | Paper | Scissors | Lizard | Spock deriving (Eq, Enum, Show)

data Rule = Rule Move Move String

instance Show Rule where
   show r = show x ++ " " ++ z ++ " " ++ show y where (Rule x y z) = r

rules = [Rule Rock Scissors "Smashes", Rule Rock Lizard "Crushes",
         Rule Paper Rock "Covers", Rule Paper Spock "Disproves",
         Rule Scissors Paper "Cuts", Rule Scissors Lizard "Decapitates",
         Rule Lizard Spock "Poisons", Rule Lizard Paper "Eats",
         Rule Spock Rock "Vapourises", Rule Spock Scissors "Smashes"]

ruleMoves :: Rule -> (Move, Move)
ruleMoves r = (a, b) where (Rule a b _) = r

matchRule :: Move -> Move -> Maybe Rule
matchRule a b = find (\r -> ruleMoves r == (a, b)) rules

matchingRule :: Move -> Move ->  Maybe Rule
matchingRule a b = matchRule a b <|> matchRule b a

newtype Name = Name String

instance Show Name where
   show n = s where (Name s) = n

data Player = Player Name

data Turn = Turn Player Move

winner :: Turn -> Turn -> Maybe (Name, Rule)
winner p1 p2 =  do r <- matchingRule p1move p2move
                   let (Rule winningMove _ _) = r
                   pure $ if p1move == winningMove
                          then (p1name, r)
                          else (p2name, r)
                where
                  (Turn (Player p1name) p1move) = p1
                  (Turn (Player p2name) p2move) = p2

playera = Player (Name "PlayerA")
playerb = Player (Name "PlayerB")

main :: IO ()
main = do case winner (Turn playera Lizard) (Turn playerb Spock) of
             Nothing -> putStrLn "Draw"
             Just (name, rule) ->
                 putStrLn $ show rule ++ ", player " ++ show name ++ " wins"
